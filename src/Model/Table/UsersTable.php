<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;

class UsersTable extends Table
{
    /* roles
     < 0 => Admins
     0  < 10 => reserved : not in use 
     10 < 20 => NCHPAD / Graids : 11 GRAIDS admin
     20 < 30 => community folks : 21 leader, 22 regular member
    */

    public function initialize(array $config)
    {
         $this->belongsToMany( 'Communities', [ 'joinTable' => 'users_communities' ] );
    }
 

    public function validationDefault(Validator $validator)
    {
        return $validator
            ->notEmpty('email', 'A valid email id is required')
            ->notEmpty('role_id', 'Please select a role')
            ->notEmpty('community_id', 'Please select a community or enter a new community name')
            ->notEmpty('first_name', 'Please enter your name');
    }

}