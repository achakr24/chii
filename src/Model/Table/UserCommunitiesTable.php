<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class UserCommunitiesTable extends Table
{

    public function initialize_broken(array $config)
       {
           $this->belongsTo('Communities', [
                   'className' => 'Publishing.Authors'
               ])
               ->setForeignKey('authorid')
               ->setProperty('person');
       }

}