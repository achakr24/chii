<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class CommunitiesTable extends Table
{

    public function initialize(array $config)
    {
        $this->belongsToMany( 'Users', [ 'joinTable' => 'users_communities' ] );
        $this->belongsTo( 'FirstLevels', [
            'foreignKey' => 'first_level', 'propertyName' => 'first_level_detail'
        ]);
        $this->hasMany( 'UsersCommunities', ['foreignKey' => 'community_id']);
    }

}