<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class FirstLevelsTable extends Table
{

    public function initialize(array $config)
    {
        $this->setPrimaryKey('id');
    }

}