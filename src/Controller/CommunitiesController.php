<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) DHD UIC. 
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistr;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class CommunitiesController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        //$this->Auth->allow();
    }

    /**
     * Welcome screen.
     *
     * @param
     * @return
     */
    public function index() {
        $q = $this->Communities->find('all');
        $q->contain(['FirstLevels'])
            ->select( [ 'members' => $q->func()->count('UsersCommunities.id')] )
            ->leftJoinWith('UsersCommunities')
            ->group(['Communities.id'])
            ->enableAutoFields(true);
        
        $this->set('communities', $q->toArray());
        $this->set('breadcrumbs', ['Communities','Explore']);
    }

    public function add(){
        $this->set('breadcrumbs', ['Communities','Add a new commuity']);
        $this->loadModel('FirstLevels');
        $first_levels = $this->FirstLevels->find( 'list', [ 'keyField' => 'id', 'valueField' => 'name' ] )->toArray();
        $this->set('first_levels', $first_levels);

    }

    public function assessment(){
        $this->set('breadcrumbs', ['Communities','Assessment submission']);
    }

    public function join($community_id)
    {
        $user = $this->Auth->user();
        
        // handle guest users
        if($user['id'] < 0) {
            $this->Flash->error('Sorry, you cannot join a community as a guest user. Please create an account first.');
        }

        if($community_id && array_key_exists($community_id, $user['communities'])) {        
            $user['community_idx'] = $community_id;
            $user['role_id'] = $user['communities'][$community_id]['_joinData']['community_role_id'];
            $this->Auth->setUser($user);
        } else {
            $this->Flash->error(__('Sorry! Thats an invalid community you are trying to switch to'));            
        }
        return $this->redirect(['controller' => 'Users', 'action'=> 'dashboard']);
    }

    public function signup() {
        if($this->Auth->user()) {
            if($this->request->query('ref') == 'explore' && $this->Auth->user()['id']==-1)
                $this->Auth->logout();
            else
                $this->redirect(['controller' => 'Users', 'action'=> 'dashboard' ]);
        }

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $postData = $this->request->getData();

            //create new communities , if passed
            $this->loadModel('Communities');
            if(count($postData['community_ids'])) {
                foreach($postData['community_ids'] as $community_id) {
                    $community = $this->Communities->find('all', [ 'conditions' => ['id' => $community_id] ])->first();    
                    if(!$community) {
                        $new_community = $this->Communities->newEntity();
                        $new_community->name = $community_id;
                        $new_community->first_level = $postData['first_level'];
                        $new_community->is_approved = false;
                        if($this->Communities->save($new_community)) {
                            // ToDo : Notify Admin
                            $user_communities[] = $new_community;
                        } else {
                            // New community could not be created, log it
                        }
                    } else {
                            $user_communities[] = $community;                        
                    }
                }
            }

            //create user
            $user = $this->Users->newEntity();
            $user->email = $postData['email'];
            $user->first_name = $postData['first_name'];
            $user->last_name = $postData['last_name'];
            $user->role_id = 22;
            $user->communities = $user_communities;
            //dump($user);

            // save user communities relationship
            if (1 || $this->Users->save($user)) {
                $this->Flash->success(__('Thank you for creating your account with us.'));
                // ToDo : Notify user to refine his community details
                $this->Auth->setUser($user);
                return $this->redirect(['controller' => 'Users', 'action'=> 'dashboard' ]);
            }
            $this->Flash->error(__('Sorry ! Something went wrong, we are looking into it. Please try again later'));
            $this->redirect([]);
        }

        $this->loadModel('FirstLevels');
        $first_levels = $this->FirstLevels->find( 'list', [ 'keyField' => 'id', 'valueField' => 'name' ] )->toArray();
        $this->set('first_levels', $first_levels);
    }

}
