<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) DHD UIC. 
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\ORM\TableRegistr;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class UsersController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Auth->allow(['index', 'explore', 'signup', 'hasPassword', 'getSecondLevels']);
    }

    /**
     * Welcome screen.
     *
     * @param
     * @return
     */
    public function index(...$path)
    {
        if($this->Auth->user())
            $this->redirect(['controller' => 'Users', 'action'=> 'dashboard' ]);

        $this->set('user',$this->Users->newEntity());

        $count = count($path);
        if (!$count) {
            return;
        }
        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        
        dump($this->Users->newEntity());die;

        //$this->set(compact('page', 'subpage'));

        /*
        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $exception) {
            if (Configure::read('debug')) {
                throw $exception;
            }
            throw new NotFoundException();
        }
        */
    }

    public function account(...$path)
    {

        $this->set('user',$this->Users->newEntity());
        $count = count($path);
        if (!$count) {
            dump('Nothing to show yet');die;
        }

        if (in_array('..', $path, true) || in_array('.', $path, true)) {
            throw new ForbiddenException();
        }

        /*
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        } */
        
        if(count($path) && $path[0]=="password") {
            $this->set('breadcrumbs', ['Account','Set Password']);

            if ($this->request->is('post')) {
                $postData = $this->request->getData();
                if(isset($postData['password']) && isset($postData['verifypassword'])) {
                    if($postData['password']==$postData['verifypassword']) {
                        $current_user = $this->Auth->user();
                        $user = $this->Users->findById($current_user['id'])->first();
                        if($user['password']==='') {
                            $user['password'] = $postData['password'];
                            $this->Users->save($user);
                            $this->Auth->setUser($user);
                            $this->Flash->success(__('Your password was successfully set. You will be asked to enter this password next time you try to log in to CHII'));
                            $this->redirect(['controller' => 'Users', 'action'=> 'dashboard' ]);
                            return;
                        }       
                    }
                }
                $this->Flash->error(__('Confirm password mismatch! Please try again'));
                //$this->set('error',__('Confirm password mismatch! Please try again'));
                $this->viewBuilder()->template('account_set_password');
            } else {
                $this->viewBuilder()->template('account_set_password');
            }
        } else {
           dump('No template to show yet');die;
        }
     
    }

    public function dashboard()
    {
        $this->set('breadcrumbs', ['Dashboard','Home']);
    }

    public function switchCommunities($community_id)
    {
        $user = $this->Auth->user();
        if($community_id && array_key_exists($community_id, $user['communities'])) {        
            $user['community_idx'] = $community_id;
            $user['role_id'] = $user['communities'][$community_id]['_joinData']['community_role_id'];
            $this->Auth->setUser($user);
        } else {
            $this->Flash->error(__('Sorry! Thats an invalid community you are trying to switch to'));
        }
        return $this->redirect(['controller' => 'Users', 'action'=> 'dashboard']);
    }


    public function login() {
        if($this->Auth->user())
            $this->redirect(['controller' => 'Users', 'action'=> 'dashboard' ]);

        if ($this->request->is('post') && $this->request->data('email')) {
            
            $user = $this->Users->find('all', ['conditions'=>['email'=> $this->request->data('email') ] ] )->contain(['Communities'])->first();
            $user_communities = $user['communities'];
            if($user) {
                if($user['password'])
                    $user = $this->Auth->identify();
                else
                    $user['no_password'] = true;
            }

            if( $user ) {
                if($user_communities) {
                    $user['communities'] = [];
                    $user['community_idx'] = '';
                    foreach($user_communities as $user_community) {
                        $user['community_idx'] = $user['community_idx'] ? $user['community_idx'] : $user_community['id'];
                        $user['communities'][$user_community['id']] = $user_community;
                    }
                    $user['role_id'] = $user['communities'][$user['community_idx']]['_joinData']['community_role_id'];
                }
                $this->Auth->setUser($user);
                return $this->redirect(['controller' => 'Users', 'action'=> 'dashboard']);
            } else {
                $this->Flash->error(__('Username or password is incorrect'));
            }
        }
        $this->set('user',$this->Users->newEntity());
    }

    public function logout( $redirect = [] ) 
    {
        if($this->Auth->user()) $this->Auth->logout();
        $this->redirect(['controller' => 'Users', 'action'=> 'index']);
    }

    public function explore( $error = null ) 
    {
        $user = [
          "id" => -1,
          "role_id" => 0,
          "email" => "guest@nchpad.org",
          "first_name" => "Guest",
          "last_name" => "User"
        ];

        $this->Auth->setUser($user);
        $this->redirect(['controller' => 'Users', 'action'=> 'dashboard']);
    }

    public function getSecondLevels() {
        
        Configure::write('debug',false);

        $this->loadComponent('RequestHandler');
        $this->RequestHandler->renderAs($this, 'json');
        $this->response->type('application/json');

        $communities = [];        
        if($this->request->getQuery('first_level')) {
            $this->loadModel('Communities');
            $_communities = $this->Communities->find( 'all', [ 
            'fields' => [ 'id', 'second_level', 'name' ],
            'conditions' => ['first_level'=> $this->request->getQuery('first_level')]
            ])->toArray();
            foreach($_communities as $val){
                $communities[$val['second_level']][$val['id']] = $val['name'];
            }
        }

        $this->response->body(json_encode($communities));
        return $this->response;
    }

    public function hasPassword($email) {
        
        Configure::write('debug',false);
        $this->loadComponent('RequestHandler');
        $this->RequestHandler->renderAs($this, 'json');
        $this->response->type('application/json');

        $hasPassword = false;
        if($this->Users->find('all', ['conditions'=>['email'=>$email,['password !='=>''],['password IS NOT'=>null] ] ] )->count())
            $hasPassword = true;

        $this->response->body(json_encode(['status'=>$hasPassword]));
        return $this->response;
    }

    public function signup() {
        if($this->Auth->user()) {
            if($this->request->query('ref') == 'explore' && $this->Auth->user()['id']==-1)
                $this->Auth->logout();
            else
                $this->redirect(['controller' => 'Users', 'action'=> 'dashboard' ]);
        }

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $postData = $this->request->getData();

            //create new communities , if passed
            $this->loadModel('Communities');
            if(count($postData['community_ids'])) {
                foreach($postData['community_ids'] as $community_id) {
                    $community = $this->Communities->find('all', [ 'conditions' => ['id' => $community_id] ])->first();    
                    if(!$community) {
                        $new_community = $this->Communities->newEntity();
                        $new_community->name = $community_id;
                        $new_community->first_level = $postData['first_level'];
                        $new_community->is_approved = false;
                        if($this->Communities->save($new_community)) {
                            // ToDo : Notify Admin
                            $user_communities[] = $new_community;
                        } else {
                            // New community could not be created, log it
                        }
                    } else {
                            $user_communities[] = $community;                        
                    }
                }
            }

            //create user
            $user = $this->Users->newEntity();
            $user->email = $postData['email'];
            $user->first_name = $postData['first_name'];
            $user->last_name = $postData['last_name'];
            $user->role_id = 22;
            $user->communities = $user_communities;
            //dump($user);

            // save user communities relationship
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Thank you for creating your account with us.'));
                // ToDo : Notify user to refine his community details
                $this->Auth->setUser($user);
                return $this->redirect(['controller' => 'Users', 'action'=> 'dashboard' ]);
            }
            $this->Flash->error(__('Sorry ! Something went wrong, we are looking into it. Please try again later'));
            $this->redirect([]);
        }

        $this->loadModel('FirstLevels');
        $first_levels = $this->FirstLevels->find( 'list', [ 'keyField' => 'id', 'valueField' => 'name' ] )->toArray();
        $this->set('first_levels', $first_levels);
    }

}
