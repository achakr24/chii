      <!-- Example DataTables Card-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Communities currently using CHII
          <a title="Add a community" href="communities/add"><i class="fa fa-plus pull-right" style="font-size: 1.6em"></i></a>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Located in</th>
                  <th>State/Territory</th>
                  <th>Members</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tfoot>
                <tr>
                  <th>Name</th>
                  <th>Located in</th>
                  <th>State/Territory</th>
                  <th>Members</th>
                  <th>Actions</th>
                </tr>
              </tfoot>
              <tbody>
                <?php foreach($communities as $community) {
                    $member = false;
                    if(isset($loggedInUser['communities']) && array_key_exists($community['id'], $loggedInUser['communities'])) $member = true;
                 ?>
                <tr <?= $member ? 'class="table-info"' : '' ?> >
                  <td><?=$community['name']?></td>
                  <td><?=$community['second_level']?></td>
                  <td><?=$community['first_level_detail']['name']?></td>
                  <td>
                  <?= $member ? ( 'You'. ( $community['members'] > 1 ? ( ' and ' . ( $community['members'] - 1 ) . ( $community['members'] > 2 ? ' others' : ' other' ) ) : '' ) ) : $community['members'] ?>
                  </td>
                  <td>
                    <a href="/communities/<?= $community['id'] ?>/entities" type="button" class="btn btn-sm">View</a>
                    <?php if($loggedInUser['id']<0) { ?>
                      <a data-toggle="modal" data-target="#requestSignUpModal" type="button" class="btn btn-sm">Join</a>
                    <?php } else if($member) { ?>
                        <a href="#" type="button" class="btn btn-sm">Manage</a>
                    <?php } else { ?>
                        <a href="/communities/join/<?=$community['id']?>" type="button" class="btn btn-sm">Join</a>
                    <?php } ?>
                  </td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </div>
        <div class="card-footer small text-muted">Data accurate as of <?= (new DateTime("now", new DateTimeZone('America/Chicago') ))->format('M d, Y H:i:s') ?> CTZ</div>
      </div>
      <div class="modal fade" id="requestSignUpModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="requestSignUpModalLabel">Sorry to interrupt !</h5>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">You need to sign up for a CHII account before you can join a community. Its FREE and will take less than minute.</div>
            <div class="modal-footer">
              <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel and continue exploring</button>
              <a class="btn btn-primary" href="/users/signup?ref=explore">Sign Up Now</a>
            </div>
          </div>
        </div>
      </div>

