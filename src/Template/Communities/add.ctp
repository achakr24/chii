<div class="card mb-3">
  <div class="card-header">
    <i class="fa fa-table"></i>
     
    </i></a>
  </div>
  <div class="container">
    <div class="card card-register mx-auto mt-5">
      <div class="card-header">Community Details</div>
      <div class="card-body">
        <form method="POST">
          <div class="form-group">
            <label for="FL">State / Territory</label>
            <div>
              <select class="select2" name="first_level" id="FL" aria-describedby="communityAddressHelp" placeholder="Select one" required> 
              <?php foreach($first_levels as $id => $val) {
                echo '<option value="'. $id . '">' . $val . '</option>';
              } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="SL">City / Region</label>
            <input name="second_level" class="form-control" id="FL" aria-describedby="communityAddressHelp" placeholder="Select City" disabled required>
          </div>
          <div class="form-group">
            <label for="communityName">Community Name</label>
            <input name="name" class="form-control" id="communityName" aria-describedby="communityNameHelp" placeholder="Enter entity name" required>
          </div>
          <button type="submit" class="btn btn-primary btn-block">Add this community</a>
        </form>
        <div class="text-center">
        </div>
      </div>
    </div>
  </div>
  <br/>
  <br/>
  <div class="card-footer small text-muted"></div>
</div>