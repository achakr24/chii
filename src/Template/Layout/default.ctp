<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CHII ';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('bootstrap.css') ?>
    <?= $this->Html->css('app.css') ?>
    
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('script'); ?>
</head>
<body> 
<div class="global-width wrap">
    <div class="custom-content-header bkgd-dark-blue">
        <?php if(isset($loggedIn) && $loggedIn) echo '<a class="pull-right" href="/users/logout"> Log Out </a>' ?>
        <!-- Searches the content of the site -->
        <!--form class="search-form" method="get" action="https://www.nchpad.org/SearchIndex.php">
            <input class="search right" name="searchKeyword" type="text" value="Search..." onfocus="if(this.value == 'Search...') { this.value = ''; }" id="search">
            <input type="hidden" name="searchin" value="articles">
            <input type="hidden" name="category" value="all">
        </form-->
        <!-- Toggles the Filter Drawer (this will not show up on the My NCHPAD page)bkgd-blue Customize Your Content  -->
        <a href="#" class="custom-content left t-white">&nbsp;</a>
    </div>
    <div class="header bkgd-white cf">
        <h1 class="logo pull-left"><a href="/" title="Home">
            <?=$this->Html->image('nchpad_logo.png', ['alt' => "NCHPAD - Building Healthy Inclusive Communities"])?>        
        </a></h1>
        <h1 class="logo pull-right"><a href="/" title="Home">
            <?=$this->Html->image('chii_logo.jpg', ['class' => "pull-right", 'alt' => "CHII - Community Health Inclusion Dashboard"])?>
        </a></h1>
    </div>

    <div class="container clearfix">
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
    </div>
    <div class="footer bkgd-med-blue cf">
        <div class="cf">
            <!-- Populate with quick links relevant to NCHPAD users (commonly used,
                 popular, etc.) -->
            <!--ul class="responsive-footer-nav">
                <li><a href="/" title="Home" class="active">Home</a></li>
                <li><a href="/Aboutus" title="About">About</a></li>
                <li><a href="/privacyPolicy" title="Privacy Policy">Privacy Policy</a></li>
                <li><a href="/Articles" title="Articles">Articles</a></li>
                <li><a href="/Directories" title="Directories">Directories</a></li>
                <li><a href="/Videos" title="Videos">Videos</a></li>
                <li><a href="/Select~Resources" title="Resources">Resources</a></li>
                <li><a href="/Contactus" title="Contact">Contact</a></li>
            </ul-->
            <ul class="col col-01">
                <li><a href="https://www.facebook.com/nchpad" target="_blank">Facebook</a></li>
                <li><a href="https://twitter.com/nchpad" target="_blank">Twitter</a></li>
                <li><a href="https://www.youtube.com/user/NCPAD" target="_blank">YouTube</a></li>
                <li><a href="http://www.pinterest.com/nchpad/" target="_blank">Pinterest</a></li>
            </ul>
            <ul class="col col-02">
                <li><a href="/Aboutus" title="About">About</a></li>
                <li><a href="/privacyPolicy" title="Privacy Policy">Privacy Policy</a></li>
                <li><a href="/Articles" title="Articles">Articles</a></li>
                <li><a href="/Directories" title="Directories">Directories</a></li>
            </ul>
            <ul class="col col-03">
                <li><a href="/Staff" title="Staff">Staff</a></li>
                <li><a href="/Videos" title="Videos">Videos</a></li>
                <li><a href="/Select~Resources" title="Resources">Resources</a></li>
                <li><a href="/Contactus" title="Contact">Contact</a></li>
            </ul>
            <ul class="col col-04">
                <li><a href="/14weeks" title="14 Weeks">14 Weeks to a healthier you!</a></li>
                <li><a href="/CRx" title="Champion's Rx">Champion's Rx</a></li>
                <li><a href="http://incfit.org" title="IncFit" target="_blank">Inclusive Fitness Coalition</a></li>
                <li><a href="http://www.uab.edu/shp/lakeshore/" title="UAB/Lakeshore Research Collaborative" target="_blank">UAB/Lakeshore Research Collaborative</a></li>
            </ul>
        </div>
        <div class="footer-main cf">
            <!-- Link to social media pages -->
            <ul class="social-media left">
                <li><a href="https://www.facebook.com/nchpad" target="_blank" class="facebook">Facebook</a></li>
                <li><a href="https://twitter.com/nchpad" target="_blank" class="twitter">Twitter</a></li>
                <li><a href="https://www.youtube.com/user/NCPAD" target="_blank" class="youtube">YouTube</a></li>
            </ul>
            <p class="right">The information provided in this website was supported by Grant/Cooperative Agreement Number U59DD000906 from the Centers for Disease Control and Prevention (CDC). The contents are solely the responsibility of the authors and do not necessarily represent the official views of CDC. 
Copyright © of The Board of Trustees of the University of Alabama  <script type="text/javascript" language="Javascript" src="/scripts6/copyright.js"></script>2017</p>
        </div>
    </div>
</div> 
<?= $this->Html->script('https://code.jquery.com/jquery-3.2.1.js', ['integrity'=>"sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE=", 'crossorigin'=>"anonymous"]) ?>
<?= $this->Html->script('bootstrap.js') ?>
<?= $this->Html->script('app.js') ?>
<?= $this->Html->css("https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css") ?>
<?= $this->Html->script("https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js") ?>
</body>
</html>
