<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'CHII ';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?= $this->Html->charset() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('font-awesome.min.css') ?>
    <?= $this->Html->css('dataTables.bootstrap4.css') ?>
    <?= $this->Html->css('sb-admin.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('script'); ?>

</head>

<body class="fixed-nav bg-dark" id="page-top">
  <!-- Navigation-->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <a class="navbar-brand" href="/users/dashboard" style="padding-top: 0px;padding-bottom: 0px;">
      <?=$this->Html->image('chii-trans.png', ['class' => "pull-left", 'height' => 40 , 'alt' => "CHII - Community Health Inclusion Dashboard"])?>
      &nbsp;&nbsp;&nbsp;&nbsp;Community Health Inclusion Dashboard
    </a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
          <a class="nav-link" href="/users/dashboard">
            <i class="fa fa-fw fa-dashboard"></i>
            <span class="nav-link-text">Dashboard</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Charts">
          <a class="nav-link" href="/communities/assessment">
            <i class="fa fa-fw fa-area-chart"></i>
            <span class="nav-link-text">Launch an assessment</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
          <a class="nav-link" href="tables.html">
            <i class="fa fa-fw fa-table"></i>
            <span class="nav-link-text">Tables</span>
          </a>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Components">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseComponents" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-wrench"></i>
            <span class="nav-link-text">Components</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseComponents">
            <li>
              <a href="navbar.html">Navbar</a>
            </li>
            <li>
              <a href="cards.html">Cards</a>
            </li>
          </ul>
        </li>
        <!--li class="nav-item" data-toggle="tooltip" data-placement="right" title="Example Pages">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseExamplePages" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-file"></i>
            <span class="nav-link-text">Example Pages</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseExamplePages">
            <li>
              <a href="login.html">Login Page</a>
            </li>
            <li>
              <a href="register.html">Registration Page</a>
            </li>
            <li>
              <a href="forgot-password.html">Forgot Password Page</a>
            </li>
            <li>
              <a href="blank.html">Blank Page</a>
            </li>
          </ul>
        </li-->
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Menu Levels">
          <a class="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti" data-parent="#exampleAccordion">
            <i class="fa fa-fw fa-sitemap"></i>
            <span class="nav-link-text">GRAIDS</span>
          </a>
          <ul class="sidenav-second-level collapse" id="collapseMulti">
            <li>
              <a href="/graids/guidelines">Guidelines</a>
            </li>
            <li>
              <a href="#">Recommendations</a>
            </li>
            <li>
              <a href="#">Adaptations</a>
            </li>
            <li>
              <a class="nav-link-collapse collapsed" data-toggle="collapse" href="#collapseMulti2">Inclusion Elements</a>
              <ul class="sidenav-third-level collapse" id="collapseMulti2">
                <li>
                  <a href="#">Third Level Item</a>
                </li>
                <li>
                  <a href="#">Third Level Item</a>
                </li>
                <li>
                  <a href="#">Third Level Item</a>
                </li>
              </ul>
            </li>
          </ul>
        </li>
        <li class="nav-item" data-toggle="tooltip" data-placement="right" title="Link">
          <a class="nav-link" href="/communities">
            <i class="fa fa-fw fa-link"></i>
            <span class="nav-link-text">Communities</span>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav sidenav-toggler">
        <li class="nav-item">
          <a class="nav-link text-center" id="sidenavToggler">
            <i class="fa fa-fw fa-angle-left"></i>
          </a>
        </li>
      </ul>
      <ul class="navbar-nav ml-auto">
        <?php if(isset($loggedInUser['communities']) && count($loggedInUser['communities'])>1) { ?>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle mr-lg-2" id="communitiesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <i class="fa fa-fw fa-users"></i>
            </a>
            <div class="dropdown-menu" aria-labelledby="alertsDropdown">
              <h6 class="dropdown-header">Switch Communities</h6>
              <?php foreach($loggedInUser['communities'] as $key => $val) { ?>
                  <div class="dropdown-divider"></div>
                  <?php if( $loggedInUser['community_idx'] == $key ) { ?>
                    <a class="dropdown-item" href="#">
                      <span class="text-success">
                        <strong>
                          <?= $loggedInUser['communities'][$key]['name']?>
                        </strong>
                      </span>
                      <span class="small float-right text-muted"></span>
                      <div class="dropdown-message small"><small>Community <?= $loggedInUser['role_id'] == 31 ? 'Leader' : 'Member' ?> since 2017</small></div>
                    </a>
                  <?php } else { ?>
                    <a class="dropdown-item" href="/users/switchCommunities/<?=$loggedInUser['communities'][$key]['id']?>">
                      <div class="dropdown-message small"><?= $loggedInUser['communities'][$key]['name']?></div>
                    </a>        
                <?php } ?>
              <?php } ?>
            </div>
          </li>
        <?php } ?>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle mr-lg-2" id="alertsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-bell"></i>
            <span class="d-lg-none">Alerts
              <span class="badge badge-pill badge-warning">6 New</span>
            </span>
            <span class="indicator text-warning d-none d-lg-block">
              <i class="fa fa-fw fa-circle"></i>
            </span>
          </a>
          <div class="dropdown-menu" aria-labelledby="alertsDropdown">
            <h6 class="dropdown-header">New Alerts:</h6>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="#">
              <span class="text-success">
                <strong>
                  <i class="fa fa-long-arrow-up fa-fw"></i>Status Update</strong>
              </span>
              <span class="small float-right text-muted">11:21 AM</span>
              <div class="dropdown-message small">This is an automated server response message. All systems are online.</div>
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item small" href="#">View all alerts</a>
          </div>
        </li>

        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="fa fa-fw fa-user"></i>
            <span class="d-lg-none">Messages
              <span class="badge badge-pill badge-primary">12 New</span>
            </span>
            <span class="indicator text-primary d-none d-lg-block">
              <i class="fa fa-fw fa-circle"></i>
            </span>
          </a>
          <div class="dropdown-menu" aria-labelledby="messagesDropdown">
            <h6 class="dropdown-header">Hello <?= $loggedInUser['first_name'] . ' ' . $loggedInUser['last_name'] ?></h6>
            <div class="dropdown-divider"></div>
            <?php if($loggedInUser['id']>0) {?>
            <a class="dropdown-item" href="#">
              <?php if($loggedInUser['role_id']==32) { ?>
                  <strong>You're a Community Member</strong>
                  <!--span class="small float-right text-muted">Member since 2017</span-->
                  <div class="dropdown-message small">Become a <strong>leader</strong> of your community. Learn More...</div>
              <?php } else { ?>
                  <strong>You're the Community Leader</strong>
                  <span class="small float-right text-muted">++</span>
                  <div class="dropdown-message small">Find out how to take charge of your community ?</div>
              <?php } ?>
            </a>
            <div class="dropdown-divider"></div>
            <?php if(isset($loggedInUser['no_password'])) { ?> 
            <a class="dropdown-item" href="/users/account/password">
              <strong>Secure your account</strong>
              <span class="small float-right text-muted"></span>
              <div class="dropdown-message small">Consider setting up a password for your account.</div>
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item small" href="#">Manage your communities</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item small" href="#">Account Settings</a>
            <?php } ?>
            <?php } else { ?>
            <a class="dropdown-item" href="/users/signup?ref=explore">
              <strong>Liking CHII ?</strong>
              <span class="small float-right text-muted"></span>
              <div class="dropdown-message small">Consider signing up for a FREE account</div>
            </a>
            <div class="dropdown-divider"></div>            
            <a class="dropdown-item" href="#">
              <strong>Learn more about CHII</strong>
              <span class="small float-right text-muted"></span>
              <div class="dropdown-message small">CHII helps you make your community more inclusive</div>
            </a>
            <?php }?>
          </div>
        </li>
        <li class="nav-item">
          <form class="form-inline my-2 my-lg-0 mr-lg-2">
            <div class="input-group">
              <input class="form-control" type="text" placeholder="Search for...">
              <span class="input-group-btn">
                <button class="btn btn-primary" type="button">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
  </nav>

  <div class="content-wrapper">

    <div class="container-fluid clearfix">
      <!-- Breadcrumbs-->
      <?php if($breadcrumbs) { ?>
        <ol class="breadcrumb">
        <?php $last_breadcrumb = array_pop($breadcrumbs);
          foreach($breadcrumbs as $breadcrumb) { ?>
            <li class="breadcrumb-item"><?= $breadcrumb ?></li>
          <?php } ?>
        <?= '<li class="breadcrumb-item active">' . $last_breadcrumb . '</li>' ?>
        </ol>
      <?php } ?>
      <?= $this->Flash->render() ?>
      <?= $this->fetch('content') ?>
    </div>

    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © NCHPAD</small>
        </div>
      </div>
    </footer>

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="/users/logout">Logout</a>
          </div>
        </div>
      </div>
    </div>
  </div>

<!-- Bootstrap core JavaScript-->
<?= $this->Html->script('jquery.js') ?>
<?= $this->Html->script('popper.min.js') ?>
<?= $this->Html->script('bootstrap.min.js') ?>
<?= $this->Html->script('jquery.easing.min.js') ?>
<?= $this->Html->script('Chart.min.js') ?>
<?= $this->Html->script('jquery.dataTables.js') ?>
<?= $this->Html->script('dataTables.bootstrap4.js') ?>
<?= $this->Html->script('sb-admin.js') ?>
<?= $this->Html->script('sb-admin-datatables.js') ?>
<?= $this->Html->script('sb-admin-charts.js') ?>

<!-- Custom scripts for this page-->
<!--script src="js/sb-admin-datatables.min.js"></script>
<script src="js/sb-admin-charts.min.js"></script-->

//<?php /*
echo $this->Html->script('app.js');
*/
echo $this->Html->css("https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css");
echo $this->Html->script("https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"); 
echo $this->Html->script('custom.js')

?>

</body>
</html>
