<?php
$class = 'message';
if (!empty($params['class'])) {
    $class .= ' ' . $params['class'];
}
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-info alert-dismissable <?= h($class) ?>" onclick="this.classList.add('hidden');" style="text-align:center">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <span class="help-block"><?= $message ?></span>
</div>

