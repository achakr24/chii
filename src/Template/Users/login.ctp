<div class="jumbotron jumbotron-codelines">
	<div class="container-lg p-responsive position-relative">
		<div class="d-md-flex flex-items-center gutter-md-spacious">
			<div class="col-md-7 text-center text-md-left pull-left">
				<h2 class="alt-h1 text-white lh-condensed-ultra mb-1 pull-left">Welcome to CHII</h2>
				<iframe width="550" height="315" src="https://www.youtube.com/embed/2XOEuR5x2bs" frameborder="0" allowfullscreen></iframe>
			</div>
			<div class="users form">
				<?= $this->Form->create($user, ['url' => ['action' => 'login'], 'id'=>'login']) ?>
			    <fieldset>
			        <legend style="text-align:right;"><?= __('Sign in to your account') ?></legend>
			        <div class="form-group">
			        <?= $this->Form->control('email', ['class'=>'form-control email-signin']) ?>
			        </div>
			        <div class="form-group hidden" id="password-div">
			        <?= $this->Form->control('password', ['class'=>'form-control', 'required'=>true]) ?>
			        </div>
			        <hr class="my-4">
				   	<div class="form-group">
					<?= $this->Form->button(__('Log in'), ['class'=>'btn btn-primary btn-large f4 btn-block']); ?>
					</div>
			        <hr class="my-4">
			        <p class="lead pull-right">
			            New to CHII ?
			            <a class="text-white" href="/users/signup">Sign up for a free account</a>
			        </p>
			        <p class="lead pull-right">
			            or
			            <a class="text-white" href="/users/explore">Explore CHII as a guest</a>
			        </p>
			   	</fieldset>
				<?= $this->Form->end() ?>
			</div>
		</div>
	</div>
</div>