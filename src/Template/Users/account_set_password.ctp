<div class="card card-login mx-auto mt-5">
  <div class="card-header">Set Password</div>
  <div class="card-body">
    <form action="/users/account/password" method="post">
      <div class="form-group has-danger">
        <label for="password">Password</label>
        <input name="password" class="form-control" id="password" type="password" placeholder="Enter password" required>
      </div>
      <div class="form-group">
        <label for="verify">Confirm Password</label>
        <input name="verifypassword" class="form-control" id="verifypassword" type="password" placeholder="Retype Password" required>
      </div>
      <div class="form-group">
        <div class="form-check">
          <label class="form-check-label"></label>
        </div>
      </div>
      <input type="submit" class="btn btn-primary btn-block">
    </form>
  </div>
</div>
