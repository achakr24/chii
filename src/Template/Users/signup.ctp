<div class="jumbotron jumbotron-codelines">
	<div class="container-lg p-responsive position-relative">
		<div class="d-md-flex flex-items-center gutter-md-spacious">
			<div class="col-md-7 text-center text-md-left pull-left">
				<h2 class="alt-h1 text-white lh-condensed-ultra mb-1 pull-left">Thank you for choosing to use CHII</h2>
			</div>
			<div class="users form">
				<?= $this->Form->create() ?>
			    <fieldset>
			        <legend style="text-align:right;"><?= __('Create your account') ?></legend>
			        <div class="form-group">
			        <?= $this->Form->control('email', ['class'=>'form-control', 'type'=>'email', 'required'=>true]) ?>
			        </div>
			        <div class="form-group">
			        <?= $this->Form->control('first_name', ['class'=>'form-control', 'required'=>true]) ?>
			        </div>
			        <div class="form-group">
			        <?= $this->Form->control('last_name', ['class'=>'form-control', 'required'=>true]) ?>
			        </div>
			        <hr class="my-4">

	                <div class="form-group">
	                <?= $this->Form->control('first_level', ['class'=>'form-control select2',
	        	        'label' => 'Select your state/territory',
	        			'options' => array_merge([''=>''], $first_levels), 'placeholder' => 'select one',
	                	'type' => 'select', 'id' => 'first-level-select']) ?>
	                </div>

	                <div class="form-group">
	                <?= $this->Form->control('community_ids', ['class'=>'form-control select2 tags',
	        	        'label' => 'Select your community', 'id' => 'second-level-select',
	        			'options' => [],  'placeholder' => 'Please select state/territory first',
	                	'multiple' => 'multiple', 'disabled' => 'disabled', 'type' => 'select']) ?>
	                </div>
			        <hr class="my-4">
			        <p class="lead">
			                 By clicking "Sign up for CHII", you agree to our
			                 <a class="text-white" href="https://www.nchpad.org/privacyPolicy" target="_blank">privacy policy</a>. <span class="js-email-notice">We may occasionally send you account related emails.</span>
			        </p>
				   	<div class="form-group">
					<?= $this->Form->button(__('Sign up for CHII'), ['class'=>'btn btn-primary btn-large f4 btn-block']); ?>
					</div>
			   	</fieldset>
				<?= $this->Form->end() ?>
			</div>
		</div>
	</div>
</div>