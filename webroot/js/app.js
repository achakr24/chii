jQuery( document ).ready(function() {  

  	// USER SIGN IN PAGE
    jQuery('#login .btn').on('click', function(event) {
          console.log(event);
          if(jQuery('#email')[0].checkValidity()) {
              if(jQuery('#email').val()) {
                  jQuery(".btn").prop('disabled',true);
                  jQuery.ajax({
                      url: "/users/hasPassword/" + jQuery('#email').val(),
                      cache: false,
                      dataType: 'json'
                  })
                  .done(function( data ) {
                      console.log(data);
                      if(data.status === true ) {
                          if( jQuery('#password-div').hasClass('hidden') ) {
                              jQuery('#password-div').removeClass('hidden');
                          } else {
                              if( jQuery('#password').val())
                                  jQuery('#login').submit()
                              else
                                jQuery('#password')[0].reportValidity()
                          }
                      } else {
                          jQuery('#password-div').addClass('hidden');
                          jQuery('#login').submit()
                      }
                      jQuery(".btn").prop('disabled',false);
                  })
                  .fail(function( data ) {
                      jQuery(".btn").prop('disabled',false);
                  });
    	        }
          }
          return true;
    });

  	// populate second levels by ajax
  	jQuery('#first-level-select').change(function() {
      console.log('First level changed')  
  		jQuery('#second-level-select').prop('disabled', true).html('');
  		if(jQuery(this).val()) {
  			var second_levels = [];
  			jQuery.ajax({
  			  url: "/users/get_second_levels",
  			  data: {'first_level':jQuery(this).val()},
  			  cache: true,
  			  dataType: 'json'
  			})
  			.done(function( data ) {
  			    var select =  jQuery( "#second-level-select" );
  			    jQuery.each(data, function(key1, value1) {
			        var optgroup = jQuery("<optgroup></optgroup>").attr("label",key1)
	  			    jQuery.each(value1, function(key2, value2) {
  				        optgroup.append(
  				        	jQuery("<option></option>").attr("value",key2).text(value2)
  				        ); 
  			    	});
  			    	select.append(optgroup);
  			    });
		  		jQuery('#second-level-select').prop('disabled', false)
		  		  .select2({tags:true,placeholder:'Select / Add your communities'});
  			})
  			.fail(function( data ) {
  			    $( "#results" ).append( html );
  			    jQuery('#second-level-select').prop('disabled', false);
  			});
  		}
  	});

    // SELECT 2
    jQuery('.select2').each(function(index) {
      jQuery(this).select2({placeholder:jQuery(this).attr('placeholder')
        //formatResult: function(state){ return "<img class='flag' src='http://select2.github.io/select2/images/flags/" + state.id.toLowerCase() + ".png'/>" + state.text },
        //formatSelection:  function(state){ return "<img class='flag' src='http://select2.github.io/select2/images/flags/" + state.id.toLowerCase() + ".png'/>" + state.text },
      });
    })



});