-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 07, 2017 at 09:08 PM
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.0.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chii`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `category_id` varchar(4) NOT NULL,
  `text` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_by` int(11) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  `is_deleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category_id`, `text`, `created_by`, `created`, `modified_by`, `modified`, `is_deleted`) VALUES
(1, 'C01', 'Active Transportation', 0, '2017-11-03 14:15:04', 0, '0000-00-00 00:00:00', 0),
(2, 'C02', 'Breastfeeding', 0, '2017-11-03 14:15:04', 0, '0000-00-00 00:00:00', 0),
(3, 'C03', 'Farmers Markets', 0, '2017-11-03 14:15:04', 0, '0000-00-00 00:00:00', 0),
(4, 'C04', 'Grocery Store Food Access', 0, '2017-11-03 14:15:04', 0, '0000-00-00 00:00:00', 0),
(5, 'C05', 'Inclusive Health Coalitions\r\n', 0, '2017-11-03 14:15:04', 0, '0000-00-00 00:00:00', 0),
(6, 'C06', 'Nutrition', 0, '2017-11-03 14:15:44', 0, '0000-00-00 00:00:00', 0),
(7, 'C07', 'Physical Activity', 0, '2017-11-03 14:15:44', 0, '0000-00-00 00:00:00', 0),
(8, 'C08', 'Public Transportation', 0, '2017-11-03 14:15:59', 0, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `chii_constructs`
--

CREATE TABLE `chii_constructs` (
  `construct_id` varchar(5) CHARACTER SET utf8 DEFAULT NULL,
  `text` text CHARACTER SET utf8
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chii_constructs`
--

INSERT INTO `chii_constructs` (`construct_id`, `text`) VALUES
('Con01', 'Transit Accessibility'),
('Con02', 'Appealing Walking Features'),
('Con03', 'Negative Walking Features'),
('Con04', 'Path Accessibility'),
('Con05', 'Intersection Accessibility'),
('Con06', 'Parking Lot Accessibility'),
('Con07', 'Entrances'),
('Con08', 'Promotional Materials'),
('Con09', 'Front Desk'),
('Con10', 'Restrooms'),
('Con11', 'Navigation'),
('Con12', 'Menus'),
('Con13', 'General Food Site Accessibility'),
('Con14', 'Affordability-Rater'),
('Con15', 'Healthy Food Promotion'),
('Con16', 'Grocery Store'),
('Con17', 'Farmers Market'),
('Con18', 'Community Garden'),
('Con19', 'Locker Rooms'),
('Con20', 'Showers'),
('Con21', 'Equipment'),
('Con22', 'General Physical Activity'),
('Con23', 'Pool'),
('Con24', 'Playground'),
('Con25', 'Multi-Use Trail'),
('Con26', 'Waiting Rooms'),
('Con27', 'Exam Room Accessibility'),
('Con28', 'Healthy Eating Policy'),
('Con29', 'Healthy Eating Programs'),
('Con30', 'Affordability-Organization'),
('Con31', 'Physical Activity Programs'),
('Con32', 'Program Material Accessibility'),
('Con33', 'Staff Training'),
('Con34', 'Staff PA Training'),
('Con35', 'Wellness Coalition'),
('Con36', 'Transportation'),
('Con37', 'School Walking Programs'),
('Con38', 'School Inclusion Policy'),
('Con39', 'School Health Promotion Policy'),
('Con40', 'Health Care'),
('Con41', 'Employee Health'),
('Con42', 'Organizational Readiness for Change'),
('Con43', 'Public Transit Availability'),
('Con44', 'Travel Training'),
('Con45', 'Transit Affordability'),
('Con46', 'Transit Information Accessibility'),
('Con47', 'Alternative Accessible Transportation'),
('Con48', 'Complete Streets Policies'),
('Con49', 'Transit-Oriented Development'),
('Con50', 'Wayfinding'),
('Con51', 'Community Accessibility'),
('Con52', 'SRTS'),
('Con53', 'Community Coalition');

-- --------------------------------------------------------

--
-- Table structure for table `communities`
--

CREATE TABLE `communities` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `first_level` char(2) NOT NULL,
  `second_level` varchar(100) NOT NULL,
  `is_approved` tinyint(1) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `communities`
--

INSERT INTO `communities` (`id`, `name`, `first_level`, `second_level`, `is_approved`, `created`) VALUES
(1, 'Little Italy', 'IL', 'Chicago', 1, '2017-10-03 16:18:03'),
(2, 'China Town', 'IL', 'Chicago', 1, '2017-10-03 16:18:03'),
(3, 'Logan Square', 'IL', 'Chicago', 1, '2017-10-03 16:19:31'),
(4, 'Illinois Medical District', 'IL', 'Chicago', 1, '2017-10-03 16:19:31'),
(5, 'Skokie', 'IL', 'Cook County', 1, '2017-10-03 16:29:36'),
(6, 'Noyes', 'IL', 'Evanston', 1, '2017-10-03 16:29:36'),
(7, 'East Town', 'WI', 'Milwaukee', 1, '2017-10-03 16:32:40'),
(8, 'Foster', 'IL', 'Evanston', 1, '2017-10-03 16:32:40'),
(33, 'NewCommunity', 'IL', '', 0, '2017-10-06 17:17:52'),
(34, 'testcommunity', 'IL', '', 0, '2017-10-17 16:31:37'),
(35, 'somecomunity', 'IL', '', 0, '2017-10-18 15:50:40'),
(36, 'NewCommunitywhichdoesnotexist', 'IL', '', 0, '2017-12-01 18:18:26');

-- --------------------------------------------------------

--
-- Table structure for table `first_levels`
--

CREATE TABLE `first_levels` (
  `id` char(2) NOT NULL,
  `type` varchar(20) NOT NULL,
  `name` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `first_levels`
--

INSERT INTO `first_levels` (`id`, `type`, `name`) VALUES
('DC', 'Federal Disctrict', 'Washington DC'),
('IL', 'State', 'Illinois'),
('PR', 'Territory', 'Puerto Rico'),
('WI', 'State', 'Wisconsin');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(2) NOT NULL,
  `name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`) VALUES
(-1, 'Super Admin'),
(11, 'GRAIDS Admin'),
(31, 'Community Leader'),
(32, 'Community Member');

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE `topics` (
  `id` int(11) NOT NULL,
  `topic_id` varchar(4) CHARACTER SET utf8 DEFAULT NULL,
  `text` text CHARACTER SET utf8
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `topics`
--

INSERT INTO `topics` (`id`, `topic_id`, `text`) VALUES
(1, 'T001', ' School/PE Training'),
(2, 'T002', 'acceesible stops and stations'),
(3, 'T003', 'accessible environment'),
(4, 'T004', 'accessible facilities'),
(5, 'T005', 'accessible fruit/veg truck'),
(6, 'T006', 'Accessible technology '),
(7, 'T007', 'accessible, alternative formats'),
(8, 'T008', 'active transportation'),
(9, 'T009', 'activity assessment'),
(10, 'T010', 'ADA access during construction'),
(11, 'T011', 'ADA requirements public transportation'),
(12, 'T012', 'adapted activities/strategies'),
(13, 'T013', 'adapted equipment'),
(14, 'T014', 'advocating for accessible, compliant transportation'),
(15, 'T015', 'afterschool programs inclusive legal mandates'),
(16, 'T016', 'alternatives to public transportaiton, innovative ride share programs'),
(17, 'T017', 'assessment'),
(18, 'T018', 'Audio/visual components'),
(19, 'T019', 'Built environment/facilities assessment'),
(20, 'T020', 'Communicating with people with disability'),
(21, 'T021', 'Community Engagement '),
(22, 'T022', 'Co-op adapted equipment'),
(23, 'T023', 'Cost'),
(24, 'T024', 'Culturally & Linguistically appropriate'),
(25, 'T025', 'customer service/communication'),
(26, 'T026', 'Emergency Preparedness'),
(27, 'T027', 'facilitation and public engagment'),
(28, 'T028', 'facilities'),
(29, 'T029', 'Farm-to School programs'),
(30, 'T030', 'Food services- delivery/pick-up'),
(31, 'T031', 'General info on Inclusive Health Coalitions '),
(32, 'T032', 'General Resouces '),
(33, 'T033', 'Grocery store access (general- multiple topics)'),
(34, 'T034', 'Guides for educators'),
(35, 'T035', 'IEP'),
(36, 'T036', 'inclusive media/advertising campaigns'),
(37, 'T037', 'Interpreter services for activities'),
(38, 'T038', 'Interviews/real life experiences of mothers breastfeeding'),
(39, 'T039', 'live inclusive cooking classes/demonstrations'),
(40, 'T040', 'maintenance of paths from snow, debris, etc.'),
(41, 'T041', 'Management training'),
(42, 'T042', 'Mantenance of transportation related equipment, elevators, etc.'),
(43, 'T043', 'Meeting room layouts'),
(44, 'T044', 'Mothers with specific disabilities'),
(45, 'T045', 'Not Listed'),
(46, 'T046', 'Overall Access'),
(47, 'T047', 'PA self-efficacy, decision making, psychology of participation '),
(48, 'T048', 'PARATRANSIT'),
(49, 'T049', 'Parking lot/passenger drop off and paths of travel on the exterior of the building'),
(50, 'T050', 'Parking lots '),
(51, 'T051', 'Parks/Fitness/recreation center membership fees'),
(52, 'T052', 'partnerships/collaborations'),
(53, 'T053', 'paths'),
(54, 'T054', 'Pedestrian paths/routes'),
(55, 'T055', 'physical access (wide aisles, counters, entrances)'),
(56, 'T056', 'playgrounds '),
(57, 'T057', 'Policy'),
(58, 'T058', 'Pools'),
(59, 'T059', 'PROWAG'),
(60, 'T060', 'Recreation/fitness staff training'),
(61, 'T061', 'resources for individuals/parents/families'),
(62, 'T062', 'Routes'),
(63, 'T063', 'Signage'),
(64, 'T064', 'Site selection'),
(65, 'T065', 'staff training'),
(66, 'T066', 'stop announcements'),
(67, 'T067', 'strategies to improve active transporation'),
(68, 'T068', 'support for mother with disability'),
(69, 'T069', 'Technology'),
(70, 'T070', 'Ticket vending machine'),
(71, 'T071', 'Tools for transportation solutions'),
(72, 'T072', 'Transportation'),
(73, 'T073', 'Travel training');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `email` varchar(254) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `password` varchar(64) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `email`, `password`, `first_name`, `last_name`, `created`, `modified`) VALUES
(1, 0, 'admin@uic.edu', '', 'Arijit', 'Chakraborty', '2017-10-06 16:51:45', '2017-10-06 17:18:51'),
(4, 22, 'achakr24_test@uic.edu', '', 'Arijit', 'Chakraborty', '2017-10-06 17:17:52', '2017-10-06 17:21:09'),
(6, 31, 'achakr24@uic.edu', '$2y$10$47Mi2wU0IUSVphM4on5UH.DVgH9g3hsUbIiSuzNM.8VpbXtCVlK86', 'Arijit', 'Chakraborty', '2017-10-06 17:24:57', '2017-10-20 14:49:47'),
(14, 31, 'aaron@chii.com', '', 'Aaron', 'Maas', '2017-10-13 15:45:39', '2017-11-20 19:36:36'),
(15, 31, 'test@test', '$2y$10$npOFKSs6/nZh1dYzwK75IO30sdZdzxoNZO/4Tm3MAO/JB.67zcwAS', 'Test', 'Test', '2017-10-13 15:45:39', '2017-10-27 15:24:16'),
(16, 22, 'new.user@chii.com', '', 'NewFirstName', 'UserLastName', '2017-12-01 15:13:49', '0000-00-00 00:00:00'),
(17, 22, 'testemail@email.com', '', 'Test', 'User', '2017-12-01 18:18:26', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users_communities`
--

CREATE TABLE `users_communities` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `community_id` int(11) NOT NULL,
  `community_role_id` int(2) NOT NULL DEFAULT '32'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_communities`
--

INSERT INTO `users_communities` (`id`, `user_id`, `community_id`, `community_role_id`) VALUES
(1, 14, 33, 32),
(2, 6, 2, 32),
(3, 6, 4, 32),
(4, 6, 1, 32),
(5, 6, 3, 31),
(6, 14, 3, 32),
(7, 15, 3, 32),
(8, 14, 2, 32),
(9, 14, 4, 32),
(10, 16, 6, 32),
(11, 17, 36, 32);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `communities`
--
ALTER TABLE `communities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `first_levels`
--
ALTER TABLE `first_levels`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `topics`
--
ALTER TABLE `topics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `users_communities`
--
ALTER TABLE `users_communities`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `communities`
--
ALTER TABLE `communities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `topics`
--
ALTER TABLE `topics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `users_communities`
--
ALTER TABLE `users_communities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
